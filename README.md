## Overview ##
Application reads .csv file with eans and requests GetMatchingProductForId 
Amazon MWS API. Response is written to PostgreSQL. After script finished, DB is 
saved to output.csv.

## Requirements ##
    .env file with following rows:
    MWS_ACCESS_KEY=...
    MWS_SECRET_KEY=...
    SELLER_ID=...
    MARKETPLACE=...

## Run ##
`docker-compose up --build`
