import logging
import os

import pandas as pd

from db import init_db, flush_records
from prepare_db import prepare_db
from amazon_mws import get_matching_product_for_id, organize_ids

logging.basicConfig(
    format="%(asctime)s - %(levelname)s - %(message)s",
    filename="kantorovich.log",
    filemode="w",
    level=logging.INFO,
)


# configs
CSV = os.path.join(os.getcwd(), "data/list_of_eans_full.csv")
BULK_SIZE = 100

conn, engine = prepare_db()
init_db(engine)

# process csv
data = pd.read_csv(CSV, dtype=str).drop_duplicates().values.transpose().tolist()[0]

result = []
chunks = organize_ids(data, 5)
for chunk in chunks:
    logging.info(f"Processed {chunk} items")
    result.extend(get_matching_product_for_id(chunk))
    if len(result) > BULK_SIZE:
        logging.info(f"Flushed {len(result)} items")
        flush_records(conn, result)
        result.clear()

if result:
    logging.info(f"Flushed {len(result)} items")
    flush_records(conn, result)
