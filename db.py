from typing import List, Dict

import sqlalchemy as sa
from sqlalchemy.engine import Connection, Engine


__all__ = ["init_db", "insert_record", "flush_records", "select_records"]


metadata = sa.MetaData()


Record = sa.Table(
    "records",
    metadata,
    sa.Column("id", sa.Integer, primary_key=True),
    sa.Column("ean", sa.String),
    sa.Column("asin", sa.String, default=''),
    sa.Column("brand", sa.String, default=''),
    sa.Column("title", sa.String, default=''),
)


def init_db(engine: Engine) -> None:
    """
    Init DB for task
    :return:
    """
    metadata.drop_all(bind=engine)
    metadata.create_all(bind=engine)


def insert_record(conn: Connection, record: Dict) -> None:
    """
    Insert single record
    :param conn: db connection
    :param record: record dict
    :return:
    """
    query = Record.insert().values(**record)
    conn.execute(query)


def flush_records(conn: Connection, records: List[dict]) -> None:
    """
    Flush records
    :param conn: db connection
    :param records: list with records
    :return:
    """
    query = Record.insert().values(records)
    conn.execute(query)


def select_records(conn: Connection) -> List[dict]:
    """
    Select records from db
    :param conn: db connection
    :return: records
    """
    query = sa.select([Record.c.ean, Record.c.asin, Record.c.brand, Record.c.title])
    result = conn.execute(query)
    return result.fetchall()
