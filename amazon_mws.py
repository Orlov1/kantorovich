import logging
import os
from typing import List, Dict, Generator
import time

import mws


AMAZON_ACCESS_KEY = os.getenv("MWS_ACCESS_KEY")
AMAZON_SECRET_KEY = os.getenv("MWS_SECRET_KEY")
SELLER_ID = os.getenv("SELLER_ID")
MARKETPLACE = os.getenv("MARKETPLACE")
PR_TYPE = "EAN"


products_api = mws.Products(
    AMAZON_ACCESS_KEY, AMAZON_SECRET_KEY, SELLER_ID, region="US"
)


def get_matching_product_for_id(ids: List[str]) -> List[dict]:
    """
    https://docs.developer.amazonservices.com/en_UK/products/Products_GetMatchingProductForId.html
    :param ids: A structured list of Id values. Used to identify products in the given marketplace.
    :return: Dict with result.
    """
    global products_api
    try:
        response = products_api.get_matching_product_for_id(
            marketplaceid=MARKETPLACE, type_=PR_TYPE, ids=ids
        ).parsed
        if isinstance(response, mws.utils.ObjectDict):
            response = [response]
        items = []
        for i, resp in enumerate(response):
            try:
                logging.info(
                    f'MWS API response status: {resp["status"]["value"]} for {next(find("Id", resp), None)["value"]} ID'
                )
                if resp["status"]["value"] == "Success":
                    products = next(find("Product", resp), None)
                    ean = next(find("Id", resp), None)["value"]
                    if not isinstance(products, list):
                        products = [products]
                    for product in products:
                        item = {"ean": ean}
                        if next(find("Title", product), None):
                            item["title"] = next(find("Title", product), None)["value"]
                        if next(find("Brand", product), None):
                            item["brand"] = next(find("Brand", product), None)["value"]
                        if next(find("ASIN", product), None):
                            item["asin"] = next(find("ASIN", product), None)["value"]
                        items.append(item)
            except TypeError as e:
                print(f"get_matching_product_for_ids TypeError: ", e)
                logging.error(f"Error GetMatchingProductForId: {str(e)}")
        return items

    except mws.MWSError as e:
        if "WRONG ITEM is not a valid" in str(e):
            logging.warning(f'WRONG ITEM is not a valid": {str(e)}')
            items = []
            for id in ids:
                item = get_matching_product_for_id([id])
                if item:
                    items.append(item)
            return items
        elif "Request is throttled" in str(e):
            time.sleep(15)
            logging.warning(f'Request is throttled": {str(e)}')
            return get_matching_product_for_id(ids)
        elif "You can have no more than 5 elements in ID list" in str(e):
            logging.warning(f"{str(e)}")
            raise ValueError("You can have no more than 5 elements in ID list")
        else:
            logging.error(f"{str(e)}")


def find(key, dictionary: Dict) -> Generator:
    try:
        for k, v in dictionary.items():
            if k == key:
                yield v
            elif isinstance(v, dict):
                for result in find(key, v):
                    yield result
            elif isinstance(v, list):
                for d in v:
                    if isinstance(d, dict):
                        for result in find(key, d):
                            yield
    except TypeError as e:
        logging.error(f"{str(e)}")


def organize_ids(ids: List[str], n: int) -> List[str]:
    """
    Organize ids in bucks of n elements. Removes duplicates. For throttling.
    :param ids: eans.
    :param n: Buck size.
    :return: organized ids.
    """
    chunks = [ids[i : i + n] for i in range(0, len(ids), n)]
    return chunks
