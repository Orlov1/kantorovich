import os
import csv

import pandas as pd

from db import select_records
from prepare_db import prepare_db


CSV = os.path.join(os.getcwd(), "data/output.csv")


conn, engine = prepare_db()
records = select_records(conn)
data = pd.DataFrame(records)
data.to_csv(
    CSV,
    header=["EAN", "ASIN", "Brand", "Title"],
    sep=",",
    encoding="iso-8859-1",
    index=False,
    quoting=csv.QUOTE_ALL,
)
