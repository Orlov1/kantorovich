FROM python:3.6

RUN curl -sSL https://raw.githubusercontent.com/sdispater/poetry/master/get-poetry.py | python

WORKDIR /app

COPY ./poetry.lock /app
COPY ./pyproject.toml /app
RUN /root/.poetry/bin/poetry config settings.virtualenvs.create false
RUN /root/.poetry/bin/poetry install -n

ENV PYTHONUNBUFFERED 1

COPY . /app