import os

import yaml
from sqlalchemy import create_engine
from sqlalchemy.engine.url import URL


CONFIG_PATH = os.path.join(os.getcwd(), "config/postgres.yaml")


def prepare_db():
    with open(CONFIG_PATH, "r") as file:
        config = yaml.load(file)

    engine = create_engine(URL(**config["postgres"]))
    conn = engine.connect()
    return conn, engine
